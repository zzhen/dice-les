# -*- coding: utf-8 -*-
"""
Created on Fri Oct 17 12:12:58 2014
Read LSM forcing file and write it into ls_flux_in
First, dictionaries are used to store values read from '***.nc' files;
Second, read data from dictionaries and write them into '***.txt' files

Top part of ls_flux_in
time	--> time(time)
shf		--> shf(time) [W m^{-2}]
lhf		--> lhf(time) [W m^{-2}]
thls	--> Tg(time) [K] "Surface skin temperature";
qts		--> 0
ps		--> psurf(time) [Pa] "Surface pressure";

Bottom part of ls_flux_in
height	--> height(lev); [m]
ug		--> Ug(time); [m/s] "East-west component of the geostrophic wind" ;
vg		--> Vg(time); [m/s] "North-south component of the geostrophic wind" ;
wfls	--> w(time, lev); [m/s] "Vertical velocity" ;
dqtdyls	--> hadvv(time, lev)/24/3600; [m/s/day] "Horizontal advection forcing of v wind" ;
dqtdxls	--> hadvu(time, lev)/24/3600; [m/s/day] "Horizontal advection forcing of u wind" ;
dqtdtls	--> hadvq(time, lev)/24/3600; [Kg/Kg/day] "Horizontal advection forcing of specific humidity" ;
dthlrad	--> hadvT(time, lev)/24/3600; [K/day] "Horizontal advection forcing of temperature" ;

@author: zzhen
"""

from netCDF4 import Dataset
#import ncdump
import numpy as np

""" ##################################################################
		1)
		store all variables
"""

#def dict_store(var_out, var_in, nm_file, var_out_new):
def dict_store(var_out, var_in, nm_file):

	var							= Dataset(nm_file, 'r')
	#print 'pf: ', var.variables['pf'][:]
	nm_out						= var_out[0] + var_out[1] + var_out[2] + var_out[3] + var_out[4]
	nm_in						= var_in[0] + var_in[1] + var_in[2] + var_in[3] + var_in[4]
	len_var						= len(nm_out)
	rlt_store					= {}

	timesteps					= len(var.variables['time'][:])

	for i in range(len_var):
		#print nm_out[i]
		if nm_out[i] == 'rts':
			rlt_store[nm_out[i]]	= var.variables[nm_in[i]][:]*1000.
		elif nm_out[i] == 'sst':
			rlt_store[nm_out[i]]	= var.variables['Tg'][:]
		elif nm_out[i] in ['dqtdtls', 'dthlrad', 'dqtdxls', 'dqtdyls']:
			# here I divided every variable (except 'wfls') by 24.*3600.,
			# but do remember that you should recover this change to [heihgt, ug, vg]
			rlt_store[nm_out[i]]	= var.variables[nm_in[i]][:]/(24.*3600.)
		elif nm_out[i] == 'pf':
			rlt_store[nm_out[i]]	= var.variables[nm_in[i]][:]/100.
		elif nm_out[i] in ['qts', 'sl']:
			rlt_store[nm_out[i]]	= var.variables['time'][:]*0.
		elif nm_out[i] == 'div':
			rlt_store[nm_out[i]]			= [0]*timesteps
			for j in range(int(2*timesteps/3.0)+1):
				rlt_store[nm_out[i]][j] 	= 5.
		else:
			rlt_store[nm_out[i]]	= var.variables[nm_in[i]][:]



	""" ###########################################
		1.1)
		Handling variables which has specific requirement
	"""
	######## Start --- Choice of different psurf, sst ########
	p_const						= 0
	ps							= 97509.453125
	if p_const == 1:
		rlt_store['psurf']	= [ps]*len(rlt_store['psurf'][:])

	######## End --- Choice of different psurf, sst ########


	""" ###########################################
		1.2)
		Transformation of 1*N dimension variable into M*N dimensions
	"""
	######## Start of Changing the value of 'height', 'ug' and 'vg' into n*m dimensions ########
	shp_var					= (len(var.variables['time']), len(var.variables['height'])) # --> (145, 70)
	print shp_var

	#for i in ['height', 'ug', 'vg']:
	for i in ['height']:
		var_store			= []
		var_store			= rlt_store[i][:]
		rlt_store[i]		= np.zeros(shp_var)
		#print var_store
		for j in range(shp_var[0]): # --> 145
			if i == 'height':
				rlt_store[i][j]	= var_store
			else:
				for k in range(shp_var[1]): # --> 70
					rlt_store[i][j,k]	= var_store[j]*(24.*3600.)

	######## End of Changing the value of 'ug' and 'vg' ########



	######## Start of writing each time-step of stored netCDF data into a dictionary ########
	key_rlt							= 'step' # name of keys in lsm

	rlt_sup							= {}
	rlt_sub							= {}
	rlt_lscale						= {}



	for i in range(shp_var[0]): # i --> 145
		key_nm_rlt					= key_rlt + str(i).zfill(3)
		""" ###########################################
			1.3)
			Generation of surface flux forcings in ls_flux_in
		"""
		#['time', 'shf', 'lhf', 'thls', 'qts', 'psurf']
		rlt_sup[key_nm_rlt]			= [0]*len(var_out[1])
		for j in var_out[1]:
			rlt_sup[key_nm_rlt][var_out[1].index(j)]			= rlt_store[j][i]
		count						= 0
		""" ###########################################
			1.4)
			Generation of large scale forcings in ls_flux_in
		"""
		#['height', 'wfls', 'dqtdtls', 'dthlrad']
		rlt_sub[key_nm_rlt]			= np.zeros((shp_var[1], len(var_out[2])))
		for k in var_out[2]:
			rlt_sub[key_nm_rlt][:, count]						= rlt_store[k][i]
			count += 1

		""" ###########################################
			1.5)
			Generation of large scale forcings in lscale_in
		"""
		rlt_lscale[key_nm_rlt]		= [0]*(len(var_out[3])+1)
		for l in var_out[3]: #['div', 'sst', 'ug', 'vg']
			rlt_lscale[key_nm_rlt][0]							= round(rlt_store['time'][i]/3600, 1)
			rlt_lscale[key_nm_rlt][var_out[3].index(l)+1]		= rlt_store[l][i]








	""" ###########################################
		1.6)
		Generation of initial conditions in sound_in
	"""
	rlt_sound						= {}
	rlt_sound[key_rlt+'000']		= [0]*len(var_out[0])
	for i in range(shp_var[1]): # i --> 70
		key_nm_rlt						= key_rlt + str(i+1).zfill(3)
		rlt_sound[key_nm_rlt]			= [0]*len(var_out[0])
		for m in var_out[0]:
			if i == 0:
				rlt_sound[key_rlt+'000'][var_out[0].index(m)]	= rlt_store[m][i]
				rlt_sound[key_nm_rlt][var_out[0].index(m)]	= rlt_store[m][i]
			else:
				rlt_sound[key_nm_rlt][var_out[0].index(m)]	= rlt_store[m][i]
	rlt_sound[key_rlt+'000'][0]			= rlt_store['pf'][0]



	""" ###########################################
		1.7)
		Generation of radiation forcings in backrad_in
		v_o4 = ['pf', 'temp', 'qv', 'o3mmr', 'sl']
	"""
	rlt_backrad							= {}
	max_backrad							= str(shp_var[1]).zfill(3)  #(145, 70)
	# Since the values in backrad_in is in a reverse sequence and the first row
	# just needs two values, (psurf, nzp), we add the first line first.
	rlt_backrad[key_rlt + max_backrad]	= [rlt_store['temp'][0], shp_var[1]]

	rcp									= 0.286 # R/c_p
	p_ref								= 1000. # reference pressure

	for i in range(shp_var[1]): # i --> 70
		######## Start of calculation of surface temperature ########
		t								= 0
		t								= rlt_store['temp'][i]*(rlt_store['pf'][i]/p_ref)**rcp
		rlt_store['temp'][i]			= t
		######## End of calculation of surface temperature ########
		key_nm_rlt						= key_rlt + str(i).zfill(3)
		rlt_backrad[key_nm_rlt]			= [0]*len(var_out[4])
		for n in var_out[4]:
			rlt_backrad[key_nm_rlt][var_out[4].index(n)]	= rlt_store[n][i]





	######## End of writing each time-step of stored netCDF data into a dictionary ########

	return rlt_sound, rlt_sup, rlt_sub, rlt_lscale, rlt_backrad

""" ##################################################################
		2)
		Output ls_flux_in.txt file
"""

def w_ls_flux(dic_sup, dic_sub, nm_write, timesteps):
	import csv
	#nm_top					= 'ls_flux_top.txt'
	with open(nm_write, "w") as output:
		writer = csv.writer(output, delimiter='\t', lineterminator='\n')
		writer.writerows([['# GCSS Dice case ls forcings and fluxes'],\
						  ['# time shf lhf thls qts ps ustar'],\
						  ['# [s] [w/m^2] [W/m^2] [K] [kg/kg] [Pa] [m/s]']
							])
		for i in range(timesteps):
			writer.writerow(dic_sup['step'+str(i).zfill(3)])
		#with open(nm_top, 'r') as topfile:
		#	output.write(topfile.read())

		output.write('\n')

		writer.writerows([['# "large_scale" "forcing" "terms"' ],\
						  ['#  height[m] w[m/s] hadvq[kg/kg/s] hadvT[K/s] hadvu[m/s/s] hadvv[m/s/s]']])
		for i in range(timesteps):
			writer.writerow([])
			writer.writerow(['# ' + str(i*1800)])
			writer.writerows(dic_sub['step'+str(i).zfill(3)])
		print '{} has been generated'.format(nm_write)


# "large_scale" "forcing" "terms"
#  height ug vg wfls dqtdxls dqtdyls dqtdtls dthlrad

""" ##################################################################
		3)
		Output lscale_in.txt and backrad_in.txt file
"""
def w_output(dic_input, nm_write, timesteps):
	import csv
	with open(nm_write, "w") as output:
		writer = csv.writer(output, delimiter='\t', lineterminator='\n')
		for i in range(timesteps):
			if nm_write == 'backrad_in.txt':
				writer.writerow(dic_input['step'+str(timesteps-i-1).zfill(3)])
			else:
				writer.writerow(dic_input['step'+str(i).zfill(3)])
		output.write('\n')
		print '{} has been generated'.format(nm_write)




if __name__ == "__main__":
	# sound_in
	v_o0					= ['hs', 'ts', 'rts', 'us', 'vs']

	# top of ls_flux_in
	v_o1					= ['time', 'shf', 'lhf', 'thls', 'qts', 'psurf', 'ustar']

	# bottom of ls_flux_in
	#v_o2					= ['height', 'ug', 'vg', 'wfls',\
	#							'dqtdxls', 'dqtdyls', 'dqtdtls', 'dthlrad']
	v_o2					= ['height', 'wfls', 'dqtdtls', 'dthlrad', 'dqtdxls', 'dqtdyls']

	# lscale_in
	v_o3					= ['div', 'sst', 'ug', 'vg']

	# backrad_in
	v_o4					= ['pf', 'temp', 'qv', 'o3mmr', 'sl']
	var_out					= [v_o0, v_o1, v_o2, v_o3, v_o4]

	# sound_in
	v_i0					= ['height', 'theta', 'qv', 'u', 'v']

	# top of ls_flux_in
	v_i1					= ['time', 'shf', 'lhf', 'Tg', 'qts', 'psurf', 'ustar']

	# bottom of ls_flux_in
	#v_i2					= ['height', 'Ug', 'Vg', 'w',\
	#							'hadvu', 'hadvv', 'hadvq', 'hadvT']
	v_i2					= ['height', 'w', 'hadvq', 'hadvT', 'hadvu', 'hadvv']

	# lscale_in; 'sst' is use 'Tg--thls' to represent in the code
	v_i3					= ['div', 'sst', 'Ug', 'Vg']

	# backrad_in
	v_i4					= ['pf', 'theta', 'qv', 'o3mmr', 'sl']
	var_in					= [v_i0, v_i1, v_i2, v_i3, v_i4]


	prefix					= 'dice_driver'
	sufix					= '.nc'


	f_in_sup 				= {}
	f_in_sub				= {}
	#seq					= str(i+1).zfill(2)
	#print 'No. of outputs: ', seq
	nm_file				= prefix + sufix

	f_sound, f_in_sup, f_in_sub, f_lscale, f_backrad = dict_store(var_out, var_in, nm_file)

	prefix_out				= 'ls_flux_in'
	sufix_out				= '.txt'
	out_nm				= prefix_out + sufix_out

	w_ls_flux(f_in_sup, f_in_sub, out_nm, 145)

	w_output(f_lscale, 'lscale_in.txt', 145)

	w_output(f_sound, 'sound_in.txt', 70+1)

	w_output(f_backrad, 'backrad_in.txt', 70+1)

	"""
	for i in range(19):
		lsm 				= {}
		lsm_store			= {}
		seq					= str(i+1).zfill(2)
		print 'No. of outputs: ', seq
		nm_file				= '../' + prefix + seq + sufix
		print 'nm_file: ', nm_file
		lsm, lsm_store		= dict_store(var_out, var_in, nm_file)
		out_nm				= prefix_out + seq + sufix_out
        print 'out_nm: ', out_nm
		write_txt(lsm, out_nm, 145)
	"""
















