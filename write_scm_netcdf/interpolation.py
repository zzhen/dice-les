# -*- coding: utf-8 -*-
"""
Created on Fri Sep 26 22:53:14 2014

Get some interpolated variables and
rescale longer data

@author: zzhen
"""

from scipy import interpolate
import numpy as np

def interp(xo, yo, xref):
	x_past, index = np.unique(xo, return_index=True, return_inverse=False)
	y_past        = yo[index] # index is a list given positions of all non-zero elements
	# If bounds_error=True, a ValueError is raised any time interpolation is attempted on a value outside of the range of x (where extrapolation is necessary).
	# If bounds_error=False, out of bounds values are assigned fill_value. By default, an error is raised.
	fl           = interpolate.interp1d(x_past, y_past, bounds_error=False, kind='linear')
	xnew, index  = np.unique(xref, return_index=True, return_inverse=False)
	#print 'x_past: ', x_past
	#print 'y_past: ', y_past
	ynew         = fl(xnew)
	return ynew