# -*- coding: utf-8 -*-
"""
Created on Fri Sep 26 22:15:43 2014
Read variables from the simulation results ***.ps/ts.nc file
Plus dice_driver.nc file
and then write out netCDF file following SCM output requirements
@author: zzhen
"""


from netCDF4 import Dataset
#import ncdump
import numpy as np
import interpolation


if __name__ == "__main__":

	fnm				= 'dice'

	""" ##################################################################
		1)
		Generating names of variables
	"""
	#  Define Time series output {time}
	v_ts			= ['time', 'lwdw', 'lwup', 'swup', 'shf', 'lhf', 'ustar', \
                       'rain', 'psurf', 'hpbl', 't2m', 'q2m', 'u10m', 'v10m', \
                       'cc'\
                       ]
	#  Defines Time Series Output, {time}
	ts_out			= {
		'time'		: ['in seconds since 19 UTC on October 23rd 1999', 's'],
		'lwdw'		: ['long wave downward radiation at surface', 'W/m2'],
		'lwup'		: ['long wave upward radiation at surface', 'W/m2'],
		'swdw'		: ['short wave downward radiation at surface', 'W/m2'],
		'swup'		: ['short wave upward radiation at surface', 'W/m2'],
		'shf'		: ['sensible heat flux', 'W/m2'],
		'lhf'		: ['latent heat flux', 'W/m2'],
		'ustar'		: ['friction velocity', 'm/s'],
		'rain'		: ['rainfall rate', 'mm/day'],
		'psurf'		: ['surface pressure', 'Pa'],
		'hpbl'		: ['boundary layer height', 'm'],
		't2m'		: ['2m temperature', 'K'],
		'q2m'		: ['2m specific humidity', 'kg/kg'],
		'u10m'		: ['10m u-component wind', 'm/s'],
		'v10m'		: ['10m v-component wind', 'm/s'],
		'cc'		: ['total cloudcover fraction', '0 1'],
	}

	#  Defines Mean State Output, {time}{levf}
	ms_out			= {
		'zf'		: ['height of full level', 'm'],
		'pf'		: ['pressure at full level', 'Pa'],
		't'			: ['temperature', 'K'],
		'th'		: ['potential temperature', 'K'],
		'q'			: ['specific humidity', 'kg/kg'],
		'u'			: ['zonal component wind', 'm/s'],
		'v'			: ['meridional component wind', 'm/s'],
	}


	#  Defines Prescribed Forcings Output, {time} ({levf} or {levh})
	pf_out		= {
		'ugeo'		: ['u-component geostrophic wind', 'm/s'],
		'vgeo'		: ['v-component geostrophic wind', 'm/s'],
		'dudt_ls'	: ['u-component momentum advection', 'm/s/s'],
		'dvdt_ls'	: ['v-component momentum advection', 'm/s/s'],
		'dtdt_ls'	: ['temperature advection', 'K/s'],
		'dqdt_ls'	: ['moisture advection', 'kg/kg/s'],
		'w'			: ['vertical movement', 'm/s'],
		}

	#  Defines Fluxes and Increments, {time} ({levf} or {levh})
	fi_out		= {
		'zh'		: ['height of half level', 'm'],
		'ph'		: ['pressure at half level', 'Pa'],
		'wt'		: ['vertical temperature flux', 'Km/s'],
		'wq'		: ['vertical moisture flux', 'kg/kg m/s'],
		'uw'		: ['vertical flux u-component momentum', 'm2/s2'],
		'vw'		: ['vertical flux v-component momentum', 'm2/s2'],
		'Km'		: ['eddy diffusivity momentum', 'm2/s'],
		'Kh'		: ['eddy diffusivity heat', 'm2/s'],
		'mf'		: ['massflux', 'kg/m2/s'],
		'dT_dt_rad'	: ['temperature tendency from radiation', 'K/s'],
		'TKE'		: ['turbulent kinetic energy', 'm^2/s^2'],
		'shear'		: ['shear production', 'm2/s3'],
		'buoy'		: ['buoyancy production', 'm2/s3'],
		'trans'		: ['total transport', 'm2/s3'],
		'dissi'		: ['dissipation', 'm2/s3'],
	}




	""" ##################################################################
    	2)
    	Creating a output netCDF file
	"""
	institute		= 'ColumbiaUniversity'
	model			= 'uclales'
	stage			= 'stage3b'
	version			= 'v10'
	lsmX			= 'lsm'

	out				= Dataset(fnm +'_scm_'+ institute+'_'+model+'_'+stage+'_'+version+'_'+lsmX + '.nc', 'w', format='NETCDF4')

	print 'type: %s'%(out.data_model)


	""" ##################################################################
		3)
		Creating global attributes
	"""
	#  Define global attributes
	def global_attri(nm_netCDF4, nm_var, var):
		for i in nm_var:
			nm_group =''
			#print general_val[general.index(i)]
			nm_group = 'nm_netCDF4.'+i    # first generates a string
			exec(nm_group + '= var[nm_var.index(i)]') # then set the string as a name of a variable
		return



	general				= ['Reference',	'ContactPerson',	'SCM_BasedModel',	'TimeStep']#, 'Surface boundary condition in stage 1']
	general_val			= ['Evaluation of Large-Eddy Simulations via Observations of Nocturnal Marine Stratocumulus',\
							'Pierre Gentine', 'Large-eddy simulation model',	'1.8069s'     ]
	global_attri(out, general, general_val)

	turbulence			= ['Scheme', 'Eddy_diffusivity_K', 'Massflux', 'Length_scale_for_EI_and_LouisType_scheme', 'K_profile']
	turbulence_val		= ['Smagorinsky model',	'unknown',	'unknown',	'unknown',	'unknown']
	global_attri(out, turbulence, turbulence_val)

	surface_bounday		= ['Surface_boundary_condition_in_stage_1']
	surface_bounday_val	= ['u* is calculated using Monin-Obukhov similarity']
	global_attri(out, surface_bounday, surface_bounday_val)






	""" ##################################################################
		4)
		Creating dimensions
	"""

	# Data read from results of UCLALES
	ps_r				= Dataset(fnm+'.ps.nc','r')
	ts_r				= Dataset(fnm+'.ts.nc','r')
	#nc_attrs, nc_dims, nc_vars = ncdump.ncdump(ts_r)

	tm_o, tm_index		= np.unique(ps_r.variables['time'][:], return_index=True, return_inverse=False)
	len_tm				= len(tm_index)
	tm					= out.createDimension('time', len_tm) # 'None' means 'Unlimited dimensions
	# Next we'll eliminate the zeros in ps_r.variables['time'], and pass its value to 'tm'
	tm					= tm_o


	levf_o, levf_index	= np.unique(ps_r.variables['zm'][:], return_index=True, return_inverse=False)
	len_levf			= len(levf_index)
	levf				= out.createDimension('levf', len_levf) # full levels
	levf				= levf_o
	#print 'levf: ', len(levf)

	levh_o, levh_index	= np.unique(ps_r.variables['zt'][:], return_index=True, return_inverse=False)
	len_levh			= len(levh_index)
	levh			= out.createDimension('levh', len_levh) # half levels
	levh			= levh_o
	#print 'levh: ', len(levh)

	levs			= out.createDimension('levs', None) # soil levels



	""" Don't delete, it's just a try
	def gen_var(dict):
		ts_store		= []
		for key in dict.keys():
			locals()[key] 			= out.createVariable(key, "f8", "time")
			locals()[key].long_name	= dict[key][0]
			locals()[key].units 	= dict[key][1]
			ts_store.append(locals()[key])
		return ts_store

	ts_var			= gen_var(ts_out)
	"""



	""" ##################################################################
		5)
		Retriving data from 'fnm.nc' and Writing data to 'out.nc'
	"""

	def final_interp(ori_ref, need_interped, new_ref):
		interped		= \
				interpolation.interp(ori_ref, need_interped, new_ref)
		return interped


	""" ###########################################
		5.1)
		Writing Time series output{time} to 'out.nc'
	"""

	for key in ts_out.keys():
		locals()[key] 			= out.createVariable(key, "f8", "time")
		locals()[key].long_name	= ts_out[key][0]
		locals()[key].units		= ts_out[key][1]
		# here put variables in ['t2m', 'q2m','u10m','v10m'], 'rain' and 'cc' as zeros
		if key[-1] in ['m', 'n', 'c']:
			locals()[key][:]	= [0]*len(tm)
		var_transit				= []
		for var in ts_r.variables:
			if key[:3] == var[:3]:
				# Because the variables in ts_output have longer length than them in ps_output.
				var_transit		= \
					final_interp(ts_r.variables['time'][:], ts_r.variables[var][:], ps_r.variables['time'][:])
				locals()[key][:]= np.asarray(var_transit)

	lwdw[:]				= final_interp(ts_r.variables['time'][:], ts_r.variables['lflxds'][:], ps_r.variables['time'][:])
	lwup[:]				= final_interp(ts_r.variables['time'][:], ts_r.variables['lflxus'][:], ps_r.variables['time'][:])
	swdw[:]				= final_interp(ts_r.variables['time'][:], ts_r.variables['sflxds'][:], ps_r.variables['time'][:])
	swup[:]				= final_interp(ts_r.variables['time'][:], ts_r.variables['sflxus'][:], ps_r.variables['time'][:])
	psurf[:]			= [97509.453125]*len(tm)



	""" ###########################################
		5.2)
		Writing Mean State {time, levf} to 'out.nc'
	"""
	for ms_key in ms_out.keys():
		if ms_key == 'zf': # 'zf' has only one dimension in 'levf'
			locals()[ms_key] 		= out.createVariable(ms_key, 'f8', 'levf')
			locals()[ms_key][:]		= levf
		else:
			locals()[ms_key] 		= out.createVariable(ms_key, 'f8', ('time', 'levf'))
		locals()[ms_key].long_name	= ms_out[ms_key][0]
		locals()[ms_key].units		= ms_out[ms_key][1]
		for var in ps_r.variables:
			if ms_key != 't' and ms_key[0] == var:
				#print 'key', ms_key
				#print 'var', var
				#print locals()[ms_key].shape
				var_transit		= np.zeros(locals()[ms_key].shape)
				# We need to project from (zt) to (zm)
				# and we have two dimensions, so we need the following loop
				for i in range(len(tm)):
					var_transit[i]		= \
							final_interp(ps_r.variables['zt'][:], ps_r.variables[var][i,:], ps_r.variables['zm'][:])
				locals()[ms_key][:] = var_transit

	# Calculation of 't' in Mean State Output
	Rd					= 287.058
	p0					= 97509.5 # Pa
	Cp					= 1003.5
	var_t				= np.zeros((len(tm), len(levf)))
	#def solve_temp(x, y): return x*(y/p0)**(Rd/Cp)
	def solve_temp(x, y): return x/(p0/y)**(Rd/Cp)
	for i in range(len(tm)):
		var_t[i]			= map(solve_temp, th[i], pf[i])
	t[:]				= var_t


	""" ###########################################
		5.3)
		Writing Prescribed Forcings {time, levf or levh} to 'out.nc'
		[ugeo(t), vgeo(t), dudt_ls(t,z), dvdt_ls(t,z), dtdt_ls(t,z), dqdt_ls(t,z), w(t,z)]
		[Ug, Vg, hadvu, hadvv, hadvT, hadvq, w] in dice_driver.nc
	"""
	################################################################################
	#### First, deal with variables need to be read from initial file ##############
	################################################################################
	list_pf				= ['ugeo', 'vgeo', 'w', 'dudt_ls', 'dvdt_ls', 'dtdt_ls', 'dqdt_ls']
	list_pf1D			= list_pf[0:2]
	list_pf2D			= list_pf[2:]
	list_pf_store		= [x + '_store' for x in list_pf]

	list_driver			= ['Ug', 'Vg', 'w', 'hadvu', 'hadvv', 'hadvT', 'hadvq']

	fnm_init			= 'dice_driver.nc'
	var					= Dataset(fnm_init, 'r')

	# because we need output every 3600s instead of 1800s in initial input
	dim_time			= len(var.variables['time'][:]) # dimensions of time
	dim_t				= int(round(dim_time/2.))
	height_init			= var.variables['height'][:]
	dim_height			= len(var.variables['height'][:]) # dimension of height


	################################################################################
	#########	Start of Creating the transit variables to store values ###########
	################################################################################
	# list_pf				= ['ugeo', 'vgeo', 'w', 'dudt_ls', 'dvdt_ls', 'dtdt_ls', 'dqdt_ls']
	for i in range(len(list_pf_store)):
		if list_driver[i] in ['Ug', 'Vg']:
			exec(list_pf_store[i] + '= [0]*dim_t')
			for j in range(dim_t):
				k				= 2*j
				locals()[list_pf_store[i]][j]			= var.variables[list_driver[i]][k]
		if list_driver[i] in ['w', 'hadvu', 'hadvv', 'hadvT', 'hadvq']:
			exec(list_pf_store[i] + '= np.zeros((dim_t, dim_height))')
			for j in range(dim_t):
				k				= 2*j
				# here I changed units from '**/day' into '**/s' with dividing by 24.*3600.,
				for m in range(dim_height):
					if list_driver[i] == 'w':
						locals()[list_pf_store[i]][j][m] = var.variables[list_driver[i]][k][m]
					else:
						locals()[list_pf_store[i]][j][m] = var.variables[list_driver[i]][k][m]/(24.*3600.)


	################################################################################
	##########			 Pass stored values to NetCDF variables  ###################
	################################################################################
	# list_pf			= ['ugeo', 'vgeo', 'w', 'dvdt_ls', 'dudt_ls', 'dtdt_ls', 'dqdt_ls']
	# list_pf2D			= ['w', 'dudt_ls', 'dvdt_ls', 'dtdt_ls', 'dqdt_ls']

	##### Interpolation of two dimensional variables in list_pf2D #####
	pf_var_list				= [[]]*len(list_pf)
	for i in range(len(list_pf)):
		pf_var_list[i]		= locals()[list_pf_store[i]]

	for i in range(len(list_pf)):
		if list_pf[i] in list_pf1D:
			locals()[list_pf[i]]			= out.createVariable(list_pf[i], 'f8', ('time'))
			var_transit							= [0]*len(locals()[list_pf[i]])
			var_transit							= pf_var_list[i]
			locals()[list_pf[i]][:]		= var_transit
		else:
			locals()[list_pf[i]]				= out.createVariable(list_pf[i], 'f8', ('time', 'levf'))
			var_transit							= np.zeros(locals()[list_pf[i]].shape)
			# We need to project from (zt) to (zm)
			# and we have two dimensions, so we need the following loop
			for j in range(len(tm)):
				var_transit[j]		= \
						final_interp(height_init, pf_var_list[i][j,:], ps_r.variables['zm'][:])
				locals()[list_pf[i]][:]		= var_transit
		locals()[list_pf[i]].long_name		= pf_out[list_pf[i]][0]
		locals()[list_pf[i]].units			= pf_out[list_pf[i]][1]



	"""

	for i in range(len(list_pf2D)):
		locals()[list_pf2D[i]]				= out.createVariable(list_pf2D[i], 'f8', ('time', 'levf'))
		locals()[list_pf2D[i]].long_name	= pf_out[list_pf[i + len(list_pf1D)]][0]
		locals()[list_pf2D[i]].units		= pf_out[list_pf[i + len(list_pf1D)]][1]
		var_transit			= np.zeros(locals()[list_pf2D[i]].shape)
		# We need to project from (zt) to (zm)
		# and we have two dimensions, so we need the following loop
		for j in range(len(tm)):
			var_transit[j]		= \
						final_interp(height_init, pf_var_list[i][j,:], ps_r.variables['zm'][:])
			locals()[list_pf2D[i]][:]		= var_transit

	#### Output one dimensional variables ['ugeo', 'vgeo'] #####
	print ugeo
	print 'i: ', locals()[list_pf[0]]

	for i in range(len(list_pf1D)):
		locals()[list_pf1D[i]]				= out.createVariable(list_pf1D[i], 'f8', ('time', 'levf'))
		locals()[list_pf1D[i]].long_name	= pf_out[list_pf[i]][0]
		locals()[list_pf1D[i]].units		= pf_out[list_pf[i]][1]
		var_transit							= [0]*len(locals()[list_pf[i]])

		print 'v: ', var_transit
		locals()[list_pf1D[i]][:]			= var_transit
	"""


	""" ###########################################
		5.4)
		Writing Fluxes and Increments {time, levf or levh} to 'out.nc'
	"""
	################################################################################
	#####  Frist write out ['Kh', 'Km', 'vw', 'uw', 'trans'] and ['wt', 'wq']  #####
	################################################################################

	for fi_key in fi_out.keys():
		if fi_key == 'zh': # 'zf' has only one dimension in 'levf'
			locals()[fi_key] 		= out.createVariable(fi_key, 'f8', 'levh')
			locals()[fi_key][:]		= levh
		elif fi_key in ['ph', 'shear', 'TKE']:
			locals()[fi_key] 		= out.createVariable(fi_key, 'f8', ('time', 'levf'))
		else:
			locals()[fi_key] 		= out.createVariable(fi_key, 'f8', ('time', 'levh'))
		locals()[fi_key].long_name	= fi_out[fi_key][0]
		locals()[fi_key].units		= fi_out[fi_key][1]
		for var in ps_r.variables:
			var_fi			= np.zeros((len_tm,len_levh)) # (73, 128)
			if fi_key.lower() == var or 'tot_'+fi_key == var: # gives ['Kh', 'Km', 'vw', 'uw', 'trans']
				# We need to just use non-repeating data, so use 'tm_index' to get the index
				count_fi		= 0
				for j in tm_index:
					var_fi[count_fi]			= ps_r.variables[var][j,:]
					count_fi					+= 1
				#print 'key', fi_key
				#print 'var', var
				locals()[fi_key][:] = var_fi
			elif 'tot_'+fi_key[-1]+fi_key[0] == var: # gives ['wt', 'wq']
				# We need to just use non-repeating data, so use 'tm_index' to get the index
				count_fi		= 0
				for j in tm_index:
					var_fi[count_fi]			= ps_r.variables[var][j,:]
					count_fi					+= 1
				#print 'elif_key1', fi_key
				#print 'elif_var1', var
				locals()[fi_key][:] = var_fi
			elif fi_key[0] == var and fi_key[1] == 'h': # gives ['ph']
				# We need to just use non-repeating data, so use 'tm_index' to get the index
				count_fi		= 0
				for j in tm_index:
					var_fi[count_fi]			= ps_r.variables[var][j,:]
					count_fi					+= 1
				#print 'elif_key2', fi_key
				#print 'elif_var2', var
				locals()[fi_key][:] = var_fi



	################################################################################
	#####     Write out some complicate variables: ['shear', 'buoy', 'TKE']    #####
	################################################################################

	# First, calculate 'TKE', TKE = sum of 0.5*u_2(time, zt), 0.5*v_2(time, zt), 0.5*w_2(time, zm),  sfs_tke(time, zm)
	# project 'u_2' and 'v_2' from dimension 'zt' to 'zm'
	interp_fi			= ['u_2_on_zm','v_2_on_zm']
	for i in interp_fi:
		k				= i[:-6]
		var_transit		= np.zeros((len(tm), len(levh)))
		for j in range(len(tm)):
			var_transit[j]		= \
						final_interp(ps_r.variables['zt'][:], ps_r.variables[k][j,:], ps_r.variables['zm'][:])
		locals()[i]	= var_transit

	w_2					= ps_r.variables['w_2'][:]

	var_tke				= np.zeros((len(tm), len(levh)))
	def sum_4(u2, v2, w2, s_tke): return u2 + v2 + w2 + s_tke
	for i in range(len(tm)):
		u_2_store			= []
		v_2_store			= []
		w_2_store			= []
		u_2_store		= [0.5*x for x in u_2_on_zm[i]]
		v_2_store		= [0.5*x for x in v_2_on_zm[i]]
		w_2_store		= [0.5*x for x in ps_r.variables['w_2'][i]]
		s_tke_store		= ps_r.variables['sfs_tke'][i]
		var_tke[i]		= map(sum_4, u_2_store, v_2_store, w_2_store, s_tke_store)
	TKE[:]					= var_tke


	# Second, calculate 'buoy' on (time, zm) and 'shear' on (time zt)
	var_buoy			= np.zeros((len_tm,len_levh)) # (73, 128)
	var_shear			= np.zeros((len_tm,len_levh)) # (73, 128)
	def sum_2(u2, v2): return u2 + v2
	count_fi		= 0
	for j in tm_index:
		var_buoy[count_fi]			= map(sum_2, ps_r.variables['boy_prd'][j,:], ps_r.variables['sfs_boy'][j,:])
		var_shear[count_fi]			= map(sum_2, ps_r.variables['shr_prd'][j,:], ps_r.variables['sfs_shr'][j,:])
		#print ps_r.variables['shr_prd'][j,1]
		#print ps_r.variables['sfs_shr'][j,1]
		#print var_shear[count_fi][1]
		count_fi					+= 1
	buoy[:]					= var_buoy
	shear[:]				= var_shear

	# Set variables ('mf' and 'dT_dt_rad') as zeros.
	mf[:]					= np.zeros((len(tm), len(levh)))
	dT_dt_rad[:]			= np.zeros((len(tm), len(levh)))

	out.close()



	""" ##################################################################
		6)
		Test for your 'out.nc'

	out_f			= Dataset(fnm +'_'+ institute+'_'+model+'_'+stage+'_'+version+'_'+lsmX + '.nc', 'r')
	ncdump.ncdump(out_f)

	out_f.close()
	"""

