# -*- coding: utf-8 -*-
"""
Created on Fri Nov 14 19:13:54 2014

@author: zzhen
"""

# from scipy.io import netcdf # it's netCDF3
from netCDF4 import Dataset
import numpy as np
import sequence
import self_plot
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt


if __name__ == "__main__":

	print "Comparison between different results, enter '0'"

	print "Comparison with Meto results, enter '1'"
	choice					= "Please enter your choice: "
	in_flg					= int(raw_input(choice))
	print "Your choice is ", in_flg

	if in_flg == 0:
		print "	Because you choose '0', please rename your two files as: "
		print "			'O-dice.ps.nc' and 'N-dice.ps.nc'"
		print "	before you run this program"


	if in_flg == 0:
		n1_file				= "Please enter the name of your 1st file \n"
		f1					= raw_input(n1_file)
		n2_file				= "Please enter the name of your 2nd file \n"
		f2					= raw_input(n2_file)
		name				= '.ps.nc'
		nm_file				= [f1+name, f2+name]
		var_t				= ['time']*2
		var_h				= ['zm']*2
		var_pt				= ['t']*2
		var_sh				= ['q']*2
		var_u				= ['u']*2
		var_v				= ['v']*2
		n_f						= len(nm_file)
		for i in range(n_f):
			if i == 0:
				f_in	= Dataset(nm_file[i],'r')
			else:
				locals()['f'+str(i)+'_in']	= Dataset(nm_file[i], 'r')
	else:
		name				= 'dice.ps.nc'
		f_nm				= 'dice_scm_MetO_stage1b.nc'
		var_t				= ['time']*2
		var_h				= ['zf', 'zm']
		var_pt				= ['th', 't']
		var_sh				= ['q', 'q']
		var_u				= ['u', 'u']
		var_v				= ['v', 'v']
		var_ut				= ['ustar']*2
		if in_flg == 1:
			name_file			= "Please enter the name of your file \n"
			f1					= raw_input(name_file)
			nm_file				= [f_nm, f1+'-'+name]
		elif in_flg == 2:
			f1					= '05'
			f2					= '10'
			f3					= '17'
			nm_file				= [f_nm, f1+'-'+name, f2+'-'+name, f3+'-'+name]
		n_f						= len(nm_file)
		for i in range(n_f):
			if i == 0:
				f_in	= Dataset(nm_file[i],'r')
			else:
				locals()['f'+str(i)+'_in']	= Dataset(nm_file[i], 'r')





	#f_in			= Dataset(f_nm,'r')
	#f1_in			= Dataset(f1_nm,'r')
	#f2_in			= Dataset(f2_nm,'r')
	#f3_in			= Dataset(f3_nm,'r')



	""" ##################################################################
		1)
		Extraction of output time index ###
	"""

	time			= f_in.variables[var_t[0]][:] # the frist two values are 0
	time1			= f1_in.variables[var_t[1]][:] # the frist two values are 0

	if in_flg == 2:
		time2			= f2_in.variables[var_t[1]][:]
		time3			= f3_in.variables[var_t[1]][:]
		time_max		= min(time[-1], time1[-1], time2[-1], time3[-1])
	else:
		time_max		= min(time[-1], time1[-1])
	#print 'time1: ', time1


	test_t			= 0
	t_start			= 0

	t_end			= int(time_max/3600+1)
	t_interval		= 4 # hours, output evry "t_inrtval" hours
	time_hrs		= range(t_start, t_end, t_interval)
	time_sec		= [x*3600 for x in time_hrs]

	time_index		= [0] #because time starts at 600 secs
	time1_index		= []
	if test_t == 1:
		#t_index	= [len_time-1 - t_interval, len_time-1]
		t_index		= [t_start, len_time-1]
	elif test_t == 0:
		pp			= PdfPages('Results.pdf')
		for i in range(len(time)):
			if time[i] in time_sec:
				time_index.append(i)
		for i in range(len(time1)):
			if time1[i] in time_sec:
				time1_index.append(i)



	""" ##################################################################
		2)
		Extraction of height -- Only plots blow 3000 m
	"""

	"""
	import interpolation

	def final_interp(ori_ref, need_interped, new_ref):
		interped		= \
				interpolation.interp(ori_ref, need_interped, new_ref)
		return interped
	"""


	max_h			= 3000 # maximum height

	zf				= []
	zf1				= []

	def cutoff(var_nm, threshold):
		len_var			= len(var_nm)
		var_st			= []
		var_st_index	= []
		for i in range(len_var):
			if var_nm[i] <= threshold:
				var_st.append(var_nm[i])
				var_st_index.append(i)
		return var_st, var_st_index

	zf, zf_index			= cutoff(f_in.variables[var_h[0]][:], max_h) # len(zf) --> 29
	zf1, zf1_index			= cutoff(f1_in.variables[var_h[1]][:], max_h) # len(zf1) --> 38
	### End --- Extraction of height ###



	""" ##################################################################
		3)
		Construction of plotting data
	"""
	def extract_data(var_nm, arr_index, col_index):
	# give the index of arrows, and the index of columns
		var_st		= np.zeros((len(arr_index),len(col_index)))
		count_arr	= 0
		for i in arr_index:
			count_col	= 0
			for j in col_index:
				var_transit		= var_nm[i][j]
				var_st[count_arr][count_col]	= var_transit
				#print j, count_col
				count_col		+=1
			count_arr	+=1
		return var_st

	""" ###########################################
		3.1)
		Generation of Potential Temperature, theta
	"""


	th_st				= f_in.variables[var_pt[0]][:] # th.shape-->(432--timesteps, 70--heights)
	th1_st				= f1_in.variables[var_pt[1]][:] # th1.shape-->(73--timesteps, 128--heights)
	th					= extract_data(th_st,time_index, zf_index)
	th1					= extract_data(th1_st,time1_index, zf1_index)


	#th1					= extract_data(f1_in.variables['t'][:],time1_index, zf1_index)
	#print 'th1: ', th1

	### End --- Construction of plotting data ###

	""" ###########################################
		3.2)
		Generation of Specific Humidity, q
	"""
	#print f_in, f1_in
	sh_st				= f_in.variables[var_sh[0]][:]
	sh1_st				= f1_in.variables[var_sh[1]][:]
	if in_flg == 0:
		sh					= extract_data(sh_st,time1_index, zf1_index)
		sh1					= extract_data(sh1_st,time1_index, zf1_index)
	else:
		sh					= [x*1000. for x in extract_data(sh_st,time_index, zf_index)]
		sh1					= extract_data(sh1_st,time1_index, zf1_index)
	#sh1					= [x/1000. for x in extract_data(sh1_st,time1_index, zf1_index)]


	#print 'sh1: ', sh1


	""" ###########################################
		3.3)
		Generation of Wind Speed, u, v
	"""

	u_st				= f_in.variables[var_u[0]][:]
	u1_st				= f1_in.variables[var_u[1]][:]
	u					= extract_data(u_st,time_index, zf_index)
	u1					= extract_data(u1_st,time1_index, zf1_index)



	v_st				= f_in.variables[var_v[0]][:]
	v1_st				= f1_in.variables[var_v[1]][:]
	v					= extract_data(v_st,time_index, zf_index)
	v1					= extract_data(v1_st,time1_index, zf1_index)




	if in_flg			== 2:
		th2_st				= f2_in.variables[var_pt[1]][:] # th1.shape-->(73--timesteps, 128--heights)
		th2					= extract_data(th2_st,time1_index, zf1_index)
		th3_st				= f3_in.variables[var_pt[1]][:] # th1.shape-->(73--timesteps, 128--heights)
		th3					= extract_data(th3_st,time1_index, zf1_index)

		sh2_st				= f2_in.variables[var_sh[1]][:]
		sh2					= extract_data(sh2_st,time1_index, zf1_index)
		sh3_st				= f3_in.variables[var_sh[1]][:]
		sh3					= extract_data(sh3_st,time1_index, zf1_index)

		u2_st				= f2_in.variables[var_u[1]][:]
		u2					= extract_data(u2_st,time1_index, zf1_index)
		u3_st				= f3_in.variables[var_u[1]][:]
		u3					= extract_data(u3_st,time1_index, zf1_index)

		v2_st				= f2_in.variables[var_v[1]][:]
		v2					= extract_data(v2_st,time1_index, zf1_index)
		v3_st				= f3_in.variables[var_v[1]][:]
		v3					= extract_data(v3_st,time1_index, zf1_index)

	### End --- Construction of plotting data ###


	# Give the title of each plot
	title_all		= []


	for i in range(len(time_hrs)):
		if in_flg == 0:
			title_all.append(str(time_hrs[i]) + ' hrs')
		else:
			title_all.append(sequence.time_index(time_hrs[i]))



	if in_flg == 2:
		nm_out		= ['MetO', f1, f2, f3]
		#theta_		= [th, th1, th2, th3]
	else:
		if in_flg == 1:
			nm_out		= ['Meto', f1]
		if in_flg == 0:
			nm_out		= [f1, f2]

	for i in range(len(time_hrs)):
		if in_flg == 2:
			theta		= {
				'x'			: [th[i], th1[i], th2[i], th3[i]],
				'y'			: [zf, zf1, zf1, zf1],
				'xlab'		: 'Potential Temperature, ' + r'$\theta, [K]$',
				'ylab'		: r'$z \ [m]$',
				'nm'		: [nm_out[0], nm_out[1], nm_out[2], nm_out[3]],
				#'title'		: r'$\theta$',
						}
			q			= {
				'x'			: [sh[i], sh1[i], sh2[i], sh3[i]],
				'y'			: [zf, zf1, zf1, zf1],
				'xlab'		: '$q \ [ g\cdot kg^{-1}]$',
				'ylab'		: '$z \ [m]$',
				'nm'		: [nm_out[0], nm_out[1], nm_out[2], nm_out[3]],
				#'title'		: r'$q$',
                    }
			wind		= {
				'x'			: [u[i], v[i], u1[i], v1[i], u2[i], v2[i]],
				'y'			: [zf, zf, zf1, zf1, zf1, zf1],
				'xlab'		: 'Wind speed, ' + r'$[m\cdot s^{-1}]$',
				'ylab'		: '$z \ [m]$',
				'nm'		: [r'$u$, ' + nm_out[0], r'$v$, ' + nm_out[0], \
							   r'$u$, ' + nm_out[1],  r'$v$, ' + nm_out[1], r'$u$, ' + nm_out[2],  r'$v$, ' + nm_out[2]],
				#'title'		: r'$$',
                    }
		else:
			theta		= {
				'x'			: [th[i], th1[i]],
				'y'			: [zf, zf1],
				'xlab'		: 'Potential Temperature, ' + r'$\theta, [K]$',
				'ylab'		: r'$z \ [m]$',
				'nm'		: [nm_out[0], nm_out[1]],
				#'title'		: r'$\theta$',
						}
			q			= {
				'x'			: [sh[i], sh1[i]],
				'y'			: [zf, zf1],
				'xlab'		: '$q \ [ g\cdot kg^{-1}]$',
				'ylab'		: '$z \ [m]$',
				'nm'		: [nm_out[0], nm_out[1]],
				#'title'		: r'$q$',
                    }
			wind		= {
				'x'			: [u[i], v[i], u1[i], v1[i]],
				'y'			: [zf, zf, zf1, zf1],
				'xlab'		: 'Wind speed, ' + r'$[m\cdot s^{-1}]$',
				'ylab'		: '$z \ [m]$',
				'nm'		: [r'$u$, ' + nm_out[0], r'$v$, ' + nm_out[0], \
							   r'$u$, ' + nm_out[1],  r'$v$, ' + nm_out[1]],
				#'title'		: r'$$',
                    }

		col_type     = ['k-', 'b--','g.', 'r-.', 'm', 'c', 'y']
		var4plot	= [theta, q, wind]
		#var4plot	= [q, theta]
		#var4plot	= [q]

		#var4plot	= [wind]
		print 'Profile at {} hours'.format(time_hrs[i])
		title         = title_all[i]
		print 'Title', title
		layout        = 'horizontal'
		if test_t == 0:
			plt.close('all')
			pp.savefig(self_plot.zz_plot(var4plot, layout, col_type, title))
		else:
			self_plot.zz_plot(var4plot, layout, col_type, title)

	if test_t == 0: pp.close()
	plt.show()