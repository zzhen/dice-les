from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import self_plot

if __name__ == "__main__":
	f_nm			= 'dice_scm_MetO_stage1b.nc'
	fd_nm			= 'dice_driver.nc'
	f1_nm			= 'dice.ps.nc'
	f1t_nm			= 'dice.ts.nc'

	f_in			= Dataset(f_nm,'r')
	fd_in			= Dataset(fd_nm,'r')
	f1_in			= Dataset(f1_nm,'r')
	f1t_in			= Dataset(f1t_nm,'r')

	var_ust				= 'ustar'
	ust					= f_in.variables[var_ust][:]
	ustd				= fd_in.variables[var_ust][:]
	ust1				= f1t_in.variables[var_ust][:]
	var_time			= 'time'
	time_ust			= f_in.variables[var_time][:]
	time_ustd			= fd_in.variables[var_time][:]
	time_ust1			= f1t_in.variables[var_time][:]

	ustar		= {
				'x'			: [time_ust, time_ust1, time_ustd],
				'y'			: [ust, ust1, ustd],
				'xlab'		: 'time [s]',
				'ylab'		: 'ustar [m/s]',
				'nm'		: ['Meto', 'LES', 'Driver'],
				#'title'		: 'Friction Speed',
                 }

	col_type     = ['k-', 'b--','g.', 'r-.', 'm', 'c', 'y']
	self_plot.zz_plot([ustar], 'horizontal', col_type, 'Friction Speed')
	plt.show()


	"""
	fig = plt.figure()
	ax = fig.add_subplot(111)
	ax.plot(time_ust, ust, label = r"Meto")
	ax.plot(time_ust1, ust1, label = r"LES")
	ax.plot(time_ustd, ustd, label = r"Driver")
	ax.legend()
	ax.set_xlabel('time [s]')
	ax.set_ylabel('ustar [m/s]')
	ax.set_title('Friction Speed')
	plt.show()
	"""