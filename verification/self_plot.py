"""
Data structures should be as similar as following examples:
var0            = {
                   'x'      : x,
                   'y'      : y1,
                   'xlab'     : '$xlabel$',
                   'ylab'     : 'ylabel',
                   'nm'     : '$x^{2}$',
                   'title'  : r'\textbf{test}'
                    }

var1            = {
                   'x'      : x,
                   'y'      : [y1, y2],
                   'xlab'     : 'xlabel',
                   'ylab'     : 'ylabel',
                   'nm'     : ['$x^{2}$', '$x^{2}+2$']
                    }

v             = [var0]
v1            = [var0, var1]
"""


import matplotlib.pyplot as plt
import numpy as np

def zz_plot(var, layout, col_type, tot_ttl=None):
	n_fig			= len(var) # gives how many figures you want to plot
	print 'n_fig: ', n_fig


	# gives the initial position of first figure
	lyt 			= ['horizontal', 'vertical']
	posit			= ['1' + str(n_fig) + '1', str(n_fig) + '11'] # e.g. [311, 131]
	wd_scale		= 6 # inches for one subfigure in width
	ht_scale		= 4 # inches for one subfigure in height
	wd				= 0 # width of whole plot region
	ht				= 0 # height of whole plot region
	if n_fig == 1:
		ini_posit	= 111
		fig			= plt.figure()
	elif n_fig < 4:
		if layout == lyt[0]:
			ini_posit	= posit[0]
			wd			= n_fig * wd_scale # 12 or 18 inches
			ht			= 2 * ht_scale # 8 inches
		elif layout == lyt[1]:
			ini_posit	= posit[1]
			wd			= 1.5 * wd_scale # 9 inches
			ht			= n_fig * ht_scale # 8 or 12 inches
		else:
			print "Wrong layout option."
			print "Please revise your layout option as 'horizontal' or 'vertical' and run the program again."
		fig              = plt.figure(figsize=(wd, ht))
	elif n_fig == 4:
		ini_posit		= 221
		wd				= 3 * (wd_scale - 1)
		ht				= 2 * (ht_scale + 1)
		fig				= plt.figure(figsize=(wd, ht))


	#print 'ini_posit', ini_posit
	fig.subplots_adjust(left=0.1, right=0.9,
					bottom=0.1, top=0.9,
					hspace=0.4, wspace=0.4)
	if tot_ttl != None: # the title for the whole plot
		fig.suptitle(tot_ttl, fontsize=14, fontweight='bold')



	for i in range(n_fig):
		ax			= fig.add_subplot(int(ini_posit)+i)
		#print 'ax', int(ini_posit)+i
		same_x		= 0
		same_y		= 0

		# Here dims_x and dims_y are used to tell how many lines will be plotted in one figure.
		dims_x		= np.array(var[i]['x']).shape
		#print 'dims_x: ', dims_x
		len_x		= len(var[i]['x'])

		if dims_x[0] > 1: same_x = True
		#print 'dims_x, same_x', dims_x, same_x

		dims_y		= np.array(var[i]['y']).shape
		len_y		= len(var[i]['y'])
		if dims_y[0] > 1: same_y = True
		#print 'dims_y, same_y', dims_y, same_y

		if same_x and not same_y:
			for ix in range(dims_x[0]): # dims_x is a two dimensions list
				#print 'ix: ', ix
				#print 'lenx: ', len(var[i]['x'][ix]), var[i]['x'][ix]
				#print 'leny: ', len(var[i]['y']), var[i]['y']
				ax.plot(var[i]['x'][ix], var[i]['y'][0], col_type[ix], label=var[i]['nm'][ix])
		elif same_y and not same_x:
			for iy in range(dims_y[0]): # dims_x is a two dimensions list
				ax.plot(var[i]['x'][0], var[i]['y'][iy], col_type[iy], label=var[i]['nm'][iy])
		elif same_x and same_y: # for var1 and varN which has different dimensions in x and y
			for j in range(dims_x[0]):
				ax.plot(var[i]['x'][j], var[i]['y'][j], col_type[j], label=var[i]['nm'][j])
		else:
			ax.plot(var[i]['x'], var[i]['y'], col_type[j], label=var[i]['nm'])

		xlab        = var[i].get('xlab', '')
		if xlab != '':
			ax.set_xlabel(xlab)

		ylab        = var[i].get('ylab', '')
		if ylab != '':
			ax.set_ylabel(ylab)

		title       = var[i].get('title', '') # title for the single plot
		if title != None:
			ax.set_title(title)

		ax.legend(loc='best')

	#plt.show()