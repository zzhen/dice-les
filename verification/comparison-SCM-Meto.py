# -*- coding: utf-8 -*-
"""
Created on Fri Nov 14 19:13:54 2014
This program used to compare the generated SCM file and standard result from Meto
@author: zzhen
"""

# from scipy.io import netcdf # it's netCDF3
from netCDF4 import Dataset
import numpy as np
import sequence
import self_plot
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt


if __name__ == "__main__":

	#fp_np		= 'dice.ps.nc'
	f_nm			= 'dice_scm_MetO_stage1b.nc'
	f1_nm			= 'dice_scm_ColumbiaUniversity_uclales_stage1b_v10_lsm.nc'

	#fp_in			= Dataset(fp_np, 'r')
	f_in			= Dataset(f_nm,'r')
	f1_in			= Dataset(f1_nm,'r')


	""" ##################################################################
		1)
		Extraction of output time index ###
	"""

	var_t			= 'time'
	time			= f_in.variables[var_t][:] # the frist two values are 0
	time1			= f1_in.variables[var_t][:] # the frist two values are 0


	test_t			= 0
	t_start			= 0
	t_end			= 73
	t_interval		= 4 # hours, output evry "t_inrtval" hours
	time_hrs		= range(t_start, t_end, t_interval)
	time_sec		= [x*3600 for x in time_hrs]

	time_index		= [0] #because time starts at 600 secs
	time1_index		= []
	if test_t == 1:
		#t_index	= [len_time-1 - t_interval, len_time-1]
		t_index		= [t_start, len_time-1]
	elif test_t == 0:
		pp			= PdfPages('Results.pdf')
		for i in range(len(time)):
			if time[i] in time_sec:
				time_index.append(i)
		for i in range(len(time1)):
			if time1[i] in time_sec:
				time1_index.append(i)


	""" ##################################################################
		2)
		Extraction of height -- Only plots blow 3000 m
	"""

	max_h			= 3000 # maximum height
	var_h			= 'zf'
	zf				= []
	zf1				= []

	def cutoff(var_nm, threshold):
		len_var			= len(var_nm)
		var_st			= []
		var_st_index	= []
		for i in range(len_var):
			if var_nm[i] <= threshold:
				var_st.append(var_nm[i])
				var_st_index.append(i)
		return var_st, var_st_index

	zf, zf_index			= cutoff(f_in.variables[var_h][:], max_h) # len(zf) --> 29
	zf1, zf1_index			= cutoff(f1_in.variables[var_h][:], max_h) # len(zf1) --> 38
	### End --- Extraction of height ###



	""" ##################################################################
		3)
		Construction of plotting data
	"""
	def extract_data(var_nm, arr_index, col_index):
		#arrs		= var_nm.shape[0]
		#cols		= var_nm.shape[1]
		var_st		= np.zeros((len(arr_index),len(col_index)))
		var_transit	= 0
		count_arr	= 0
		for i in arr_index:
			count_col	= 0
			for j in col_index:
				var_transit		= var_nm[i][j]
				var_st[count_arr][count_col]	= var_transit
				#print j, count_col
				count_col		+=1
			count_arr	+=1
		return var_st

	""" ###########################################
		3.1)
		Generation of Potential Temperature, theta
	"""
	var_pt				= 'th'
	th_st				= f_in.variables[var_pt][:] # th.shape-->(432--timesteps, 70--heights)
	th1_st				= f1_in.variables[var_pt][:] # th1.shape-->(73--timesteps, 128--heights)
	th					= extract_data(th_st,time_index, zf_index)
	th1					= extract_data(th1_st,time1_index, zf1_index)
	#th1					= extract_data(f1_in.variables['t'][:],time1_index, zf1_index)

	### End --- Construction of plotting data ###

	""" ###########################################
		3.2)
		Generation of Specific Humidity, q
	"""
	var_sh				= 'q'
	sh_st				= f_in.variables[var_sh][:]
	sh1_st				= f1_in.variables[var_sh][:]
	#sh					= extract_data(sh_st,time_index, zf_index)
	sh					= [x*1000. for x in extract_data(sh_st,time_index, zf_index)]
	sh1					= extract_data(sh1_st,time1_index, zf1_index)
	#sh1					= [x/1000. for x in extract_data(sh1_st,time1_index, zf1_index)]


	""" ###########################################
		3.3)
		Generation of Wind Speed, u, v
	"""
	var_u				= 'u'
	u_st				= f_in.variables[var_u][:]
	u1_st				= f1_in.variables[var_u][:]
	u					= extract_data(u_st,time_index, zf_index)
	u1					= extract_data(u1_st,time1_index, zf1_index)

	var_v				= 'v'
	v_st				= f_in.variables[var_v][:]
	v1_st				= f1_in.variables[var_v][:]
	v					= extract_data(v_st,time_index, zf_index)
	v1					= extract_data(v1_st,time1_index, zf1_index)


	### End --- Construction of plotting data ###


	# Give the title of each plot
	title_all		= []
	for i in range(len(time_hrs)):
		title_all.append(sequence.time_index(time_hrs[i]))

	"""
	fig = plt.figure()
	ax = fig.add_subplot(111)
	ax.plot(th[1], zf, label = r"This is \textbf{th}")
	ax.plot(th1[1], zf1, label = r"This is \textit{th1}")
	ax.legend()
	plt.show()
	"""

	col_type     = ['k-', 'b--','g.', 'r-.', 'm', 'c', 'y']

	for i in range(len(time_hrs)):
		theta		= {
			'x'			: [th[i], th1[i]],
			'y'			: [zf, zf1],
			'xlab'		: 'Potential Temperature, ' + r'$[K]$',
			'ylab'		: r'$z \ [m]$',
			'nm'		: [r'$\theta$' + ', MetO', r'$\theta$' + ', LES'],
			#'title'		: r'$\theta$',
						}
		q			= {
			'x'			: [sh[i], sh1[i]],
			'y'			: [zf, zf1],
			'xlab'		: '$q \ [ g\cdot kg^{-1}]$',
			'ylab'		: '$z \ [m]$',
			'nm'		: [r'$q$' + ', MetO', r'$q$' + ', LES'],
			#'title'		: r'$q$',
                    }
		wind		= {
			'x'			: [u[i], v[i], u1[i], v1[i]],
			'y'			: [zf, zf, zf1, zf1],
			'xlab'		: 'Wind speed, ' + r'$[m\cdot s^{-1}]$',
			'ylab'		: '$z \ [m]$',
			'nm'		: [r'$u$' + ', MetO', r'$v$' + ', MetO', r'$u$' + ', LES',  r'$v$' + ', LES'],
			#'title'		: r'$$',
                    }

		var4plot	= [theta, q, wind]
		#var4plot	= [q, theta]
		#var4plot	= [q]

		#var4plot	= [wind]

		print 'Profile at {} hours'.format(time_hrs[i])
		title         = title_all[i]
		print 'Title', title
		layout        = 'horizontal'
		if test_t == 0:
			plt.close('all')
			pp.savefig(self_plot.zz_plot(var4plot, layout, col_type, title))
		else:
			self_plot.zz_plot(var4plot, layout, title)

	if test_t == 0: pp.close()
	plt.show()
