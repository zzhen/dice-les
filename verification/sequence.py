def time_index(time):
	day_crit     = [-14, 10, 34, 58, 72+1]
	date         = ['23 Oct', '24 Oct', '25 Oct', '26 Oct']
	guess_left   = 0
	day          = 0
	hrs          = 0
	when         = ''


	while guess_left < 4:
		if day_crit[guess_left] < time <= day_crit[guess_left+1]:
			day = date[guess_left]
			hrs = time - day_crit[guess_left]
			if hrs < 10:
				when = str(0)+str(hrs)
			else:
				when = str(hrs)
			break
		guess_left += 1


	total_nm = day + ', ' + when + ':00 CDT'
	#print 'total_nm: ', total_nm

	return total_nm